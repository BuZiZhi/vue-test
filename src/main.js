// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import App from './App';
import router from './router';
import './common/stylus/index.styl';
import axios from 'axios';
Vue.config.productionTip = false;
Vue.prototype.$ajax = axios;
const app = new Vue({
  router,
  render: h => h(App)
});
app.$mount('#app');
